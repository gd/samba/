# Minimum and exact required versions for various libraries 
# if we use the ones installed in the system.
define(TDB_MIN_VERSION,1.1.7)
define(TALLOC_MIN_VERSION,2.0.0)
define(LDB_REQUIRED_VERSION,0.9.9)
define(TEVENT_REQUIRED_VERSION,0.9.8)
